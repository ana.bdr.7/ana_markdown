1. lo primero creamos el repositorio en gitlab ana_markdown
2. clonamos este repositorio en local, en este caso en /var/www con el comando git clone https://gitlab.com/ana.bdr.7/ana_markdown.git
3. añadimos el documento readme.md con el comando git add README.md
![Titulo](imagenes/git01.png)
4. creamos el primer commit con el mensaje "Primer commit de Ana" y comando git commit -m "Primer commit de Ana" 
![Titulo](imagenes/git02.png)
5. Subimos los cambios al repositorio remoto
![Titulo](imagenes/git03.png)
6. creamos archivo.txt y carpeta privada y les añadimos a gitignore para ello creamos la carpeta .gitignore e inlcuimos 
los archivos que queremos ignorar
![Titulo](imagenes/git04.png)
![Titulo](imagenes/git05.png)
![Titulo](imagenes/git06.png)
7. creamos el archivo ana.md
![Titulo](imagenes/git07.png)
8. creamos un tag v0.1
![Titulo](imagenes/git08.png)
9. creamos una tabla en el documento anterior
![Titulo](imagenes/git09.png)
![Titulo](imagenes/git10.png)
10. creamos una nueva rama
![Titulo](imagenes/git11.png) 
11. nos ubicamos en la nueva rama creamos un documento de despliegue.md y realizamos un commit y subimos los cambios al repositorio remoto
![Titulo](imagenes/git12.png)
![Titulo](imagenes/git13.png)
12. volvemos a master y creamos un merge con rama-Ana
![Titulo](imagenes/git14.png)
13. modificamos en ambas ramas el mismo archivo con distinto contenido, al realizar el merge entrará en conflicto.
Para solucionarlo solo hay que modificar el archivo y realizar un commit
![Titulo](imagenes/git15.png)
![Titulo](imagenes/git16.png)
![Titulo](imagenes/git17.png)
![Titulo](imagenes/git18.png)
![Titulo](imagenes/git20.png)
![Titulo](imagenes/git21.png)
14. creamos un tag v0.2 y borramos la rama-Ana
![Titulo](imagenes/git22.png)

